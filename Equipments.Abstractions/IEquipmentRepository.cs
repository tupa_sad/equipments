﻿using Equipments.Abstractions.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Equipments.Abstractions
{
    public interface IEquipmentRepository
    {
        Task<IEnumerable<Equipment>> GetAllEquipments(int page, int perPage);
        Task<Equipment> GetEquipment(string equipmentId);
        Task<Equipment> CreateEquipmentAsync(Equipment equipment);
        Task DeleteEquipmentAsync(Equipment equipment);
    }
}
