﻿using Equipments.Abstractions.Models;
using Equipments.Abstractions.Requests;
using Equipments.Abstractions.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Equipments.Abstractions
{
    public interface IEquipmentService
    {
        Task<IList<EquipmentResponse>> GetAllAsResponses(int page, int perPage);
        Task<Equipment> Get(string equipmentId);
        Task<Equipment> Create(Equipment equipment);
        Task Delete(string equipmentId);
        Task<Equipment> Update(UpdateEquipmentRequest updateEquipmentRequest);
    }
}
