﻿using Helpers;

namespace Equipments.Abstractions.Models
{
    public partial class Equipment : HaveNameDescription
    {
        public string EquipmentId { get; set; }
    }
}
