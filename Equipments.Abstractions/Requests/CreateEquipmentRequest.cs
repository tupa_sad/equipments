﻿namespace Equipments.Abstractions.Requests
{
    public class CreateEquipmentRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsCompanyOwnership { get; set; }
    }
}
