﻿using Equipments.Abstractions.Models;

namespace Equipments.Abstractions.Requests
{
    public class UpdateEquipmentRequest
    {
        public string EquipmentId { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }

        public void UpdateEquiment(ref Equipment equipment)
        {
            equipment.Name = Name ?? equipment.Name;
            equipment.Description = Descriprion ?? equipment.Description;
        }
    }
}
