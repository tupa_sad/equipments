﻿using Equipments.Abstractions.Models;

namespace Equipments.Abstractions.Responses
{
    public class EquipmentResponse
    {
        public EquipmentResponse(Equipment equipment)
        {
            EquipmentId = equipment.EquipmentId;
            Name = equipment.Name;
            Description = equipment.Description;
        }

        public string EquipmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
