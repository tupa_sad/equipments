﻿using Equipments.Abstractions.Models;
using Microsoft.EntityFrameworkCore;

namespace Equipments.Api.Context
{
    public class EquipmentContext : DbContext
    {
        public EquipmentContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Equipment> Equipments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.ToTable("equipment");

                entity.Property(e => e.EquipmentId)
                .HasColumnName("equipment_id")
                .HasMaxLength(36);

                entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

                entity.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);
            });
        }
    }
}
