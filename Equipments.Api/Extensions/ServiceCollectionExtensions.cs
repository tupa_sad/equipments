﻿using Equipments.Api.Context;
using Equipments.Api.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace Equipments.Abstractions.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitializeEquipmentsServices(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> options = null)
        {
            services.AddDbContext<EquipmentContext>(options);
            services.TryAddScoped<IEquipmentRepository, EquipmentRepository>();
            services.TryAddScoped<IEquipmentService, EquipmentService>();
            return services;
        }
    }
}
