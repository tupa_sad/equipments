﻿using Equipments.Abstractions;
using Equipments.Abstractions.Models;
using Equipments.Api.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equipments.Api.Services
{
    public class EquipmentRepository : IEquipmentRepository
    {
        private readonly EquipmentContext context;

        public async Task<IEnumerable<Equipment>> GetAllEquipments(int page, int perPage)
        {
            IQueryable<Equipment> request = context.Equipments;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            request = request.OrderBy(x => x.Name);
            return await request.ToListAsync();
        }

        public async Task<Equipment> GetEquipment(string equipmentId)
        {
            Equipment equipment = context.Equipments.FirstOrDefault(x => x.EquipmentId == equipmentId);
            if (equipment == null)
            {
                throw new Exception($"Equipment with '{equipmentId}' id is not found. ({nameof(EquipmentRepository)}.{nameof(GetEquipment)})");
            }
            return equipment;
        }

        public async Task<Equipment> CreateEquipmentAsync(Equipment equipment)
        {
            context.Equipments.Add(equipment);
            await context.SaveChangesAsync();
            return equipment;
        }

        public async Task DeleteEquipmentAsync(Equipment equipment)
        {
            context.Remove(equipment);
            await context.SaveChangesAsync();
        }
    }
}
