﻿using Equipments.Abstractions;
using Equipments.Abstractions.Models;
using Equipments.Abstractions.Requests;
using Equipments.Abstractions.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Equipments.Api.Services
{
    public class EquipmentService : IEquipmentService
    {
        private readonly EquipmentRepository equipmentRepository;

        public EquipmentService(EquipmentRepository equipmentRepository)
        {
            this.equipmentRepository = equipmentRepository;
        }

        public async Task<IList<EquipmentResponse>> GetAllAsResponses(int page, int perPage)
        {
            var equipmentsDb = await equipmentRepository.GetAllEquipments(page, perPage);

            var equipments = new List<EquipmentResponse>();
            foreach (var equipmentDb in equipmentsDb)
            {
                equipments.Add(new EquipmentResponse(equipmentDb));
            }

            return equipments;
        }

        public async Task<Equipment> Get(string equipmentId)
        {
            Equipment equipment = await equipmentRepository.GetEquipment(equipmentId);
            if (equipment == null)
            {
                return null;
            }
            return equipment;
        }

        public async Task<Equipment> Create(Equipment equipment)
        {
            return await equipmentRepository.CreateEquipmentAsync(equipment);
        }

        public async Task Delete(string equipmentId)
        {
            Equipment equipment = await equipmentRepository.GetEquipment(equipmentId);
            if (equipment != null)
            {
                await equipmentRepository.DeleteEquipmentAsync(equipment);
            }
            return;
        }

        public async Task<Equipment> Update(UpdateEquipmentRequest updateEquipmentRequest)
        {
            Equipment equipment = await equipmentRepository.GetEquipment(updateEquipmentRequest.EquipmentId);
            updateEquipmentRequest.UpdateEquiment(ref equipment);
            return equipment;
        }
    }
}
